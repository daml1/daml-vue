export type User = {
    userId: string;
    primaryParty?: string
}

export type UserOption = {
    label: string;
    value: string;
}
  