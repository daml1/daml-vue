import { defineStore } from 'pinia';
// import * as damlTypes from '@daml/types';
import * as damlLedger from '@daml/ledger';
import { User, UserOption } from '../types/daml-type';


const blankJwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwczovL2RhbWwuY29tL2xlZGdlci1hcGkiOnsibGVkZ2VySWQiOiJzYW5kYm94IiwiYXBwbGljYXRpb25JZCI6ImZvb2JhciIsImFjdEFzIjpbIiJdfX0.j9Pmkts4J4CGAwpe6ORCohgd1_SLGljf-wF1a7Bvn9k';
const endpoint = 'http://localhost:8080/';



export const useDamlStore = defineStore('daml', {
  state: () => ({
    currentUser: '' as string,
    users: [] as User[],
    ledger: new damlLedger.default({
      token: blankJwt as string,
      httpBaseUrl: endpoint as string,
    }),
    jwt: blankJwt as string,
  }),
  getters: {
    userOptions() {
      const option: UserOption[] = this.users.map((u) => ({ label: u.userId, value: u.primaryParty as string }));
      return option;
    },
    getPublicUser() {
      const users = this.users as any[];
      return users.find((u) => u.userId === 'public');
    }
  },
  actions: {
    async getUsers() {
      const users = await this.ledger.listUsers();
      this.users = users;
      users.map((user) => {
        if (!user.primaryParty) {
          user.primaryParty = ''
        }
      });
    },
    async createUser(name: string) {
      const party = await this.ledger.allocateParty({ displayName: name });

      const myHeaders = new Headers();
      myHeaders.append('Authorization', `Bearer ${this.jwt}`);
      myHeaders.append('Content-Type', 'application/json');
      
      const raw = JSON.stringify({
        'userId': name,
        'primaryParty': party.identifier,
        'rights': [
          {
            'type': 'CanActAs',
            'party': party.identifier
          },
          {
            'type': 'CanReadAs',
            'party': party.identifier
          }
        ]
      });
      
      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
      };
      
      await fetch(`${endpoint}v1/user/create`, requestOptions);


      await this.updateJwt(party.identifier);

      const myHeaders2 = new Headers();
      myHeaders2.append('Authorization', `Bearer ${this.jwt}`);
      myHeaders2.append('Content-Type', 'application/json');

      const raw2 = JSON.stringify({
        'templateId': 'User:Alias',
        'payload': {
          'username': party.identifier,
          'alias': name,
          'public': this.getPublicUser.primaryParty
        }
      });

      const requestOptions2 = {
        method: 'POST',
        headers: myHeaders2,
        body: raw2,
      };

      console.log(raw2)

      await fetch(`${endpoint}v1/create`, requestOptions2);
      
      await this.getUsers();
    },
    async queryAllContracts() {
      const myHeaders = new Headers();
      myHeaders.append('Authorization', `Bearer ${this.jwt}`);
      myHeaders.append('Content-Type', 'application/json');
      
      const requestOptions = {
        method: 'GET',
        headers: myHeaders
      };
      
      const res = await fetch(`${endpoint}v1/query`, requestOptions);
      const data = await res.json();

      return data.result;
    },
    async queryContracts(jsonStr: string) {
      const myHeaders = new Headers();
      myHeaders.append('Authorization', `Bearer ${this.jwt}`);
      myHeaders.append('Content-Type', 'application/json');
      
      const requestOptions = {
        method: 'POST',
        body: jsonStr,
        headers: myHeaders
      };
      
      const res = await fetch(`${endpoint}v1/query`, requestOptions);
      const data = await res.json();

      return data.result;
      
    },
    async updateJwt(party: string) {

      const myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      const raw = JSON.stringify({
        'party': party
      });
      const requestOptions = {
        method: 'POST',
        body: raw,
        headers: myHeaders
      };

      const res = await fetch('http://localhost:4000/token', requestOptions);
      const tokenJson = await res.json();
      this.ledger = new damlLedger.default({
        token: tokenJson.token as string,
        httpBaseUrl: endpoint as string,
      });
      this.jwt = tokenJson.token;

    },
    async archiveContract(contractId: string, templateId: string) {
      const myHeaders = new Headers();
      myHeaders.append('Authorization', `Bearer ${this.jwt}`);
      myHeaders.append('Content-Type', 'application/json');
      
      const raw = JSON.stringify({
        'templateId': templateId,
        'contractId': contractId,
        'choiceInterfaceId': templateId,
        'choice': 'Archive',
        'argument': {}
      });
      
      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
      };
      
      await fetch(`${endpoint}v1/exercise`, requestOptions);
    }
    

  },
});
